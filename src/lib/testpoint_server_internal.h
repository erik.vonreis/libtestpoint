//
// Created by erik.vonreis on 9/27/23.
//

#ifndef LIBTESTPOINT_SERVER_INTERNAL_H
#define LIBTESTPOINT_SERVER_INTERNAL_H

/* -------------------------------- */
/* find an unused client id struct  */
/* mark it as used and return the   */
/* index to it.                     */
/* made public for direct access    */
/* to a client id                   */
/* -------------------------------- */
int allocate_client_id();

/**
 *
 * @return the server node (DCUID)
 */
int getNode();

/**
 * @brief Update the active testpoints in the active_block based on the
 * interface, slot, and testpoint value provided.
 *
 * @param interface The interface type (TP_EX_INTERFACE or TP_TP_INTERFACE).
 * @param slot The slot number to update.
 * @param tp The testpoint value to set.
 * @return 1 if the update was successful, 0 otherwise.
 */
int update_active_testpoints(int interface, int slot, testpoint_t tp);

#define _KEEPALIVE_TIMEOUT 60
#define _KEEP_AROUND 240

#endif // LIBTESTPOINT_SERVER_INTERNAL_H
