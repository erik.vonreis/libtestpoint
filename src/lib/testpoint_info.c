static char *versionId = "Version $Id$";
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: testpointinfo						*/
/*                                                         		*/
/* Module Description: utility functions for handling test points	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
/*
#ifndef DEBUG
#define DEBUG
#endif
*/

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Includes: 								*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#include "testpoint_info.h"
#include "testpoint_interface.h"
#include <stdlib.h>
#include <string.h>

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: tpIsValid					*/
/*                                                         		*/
/* Procedure Description: gets a test point index			*/
/*                                                         		*/
/* Procedure Arguments: channel info, node (return), test point (return)*/
/*                                                         		*/
/* Procedure Returns: 1 if test point, 0 otherwise			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int testpoint_is_valid(const gdsChnInfo_t *chn, int *node, testpoint_t *tp) {
  /* test channel */
  if (chn == NULL) {
    return 0;
  }

  /* is test point? */
  if ((IS_TP(chn)) && (TP_ID_TO_INTERFACE(chn->chNum) >= 0)) {
    /* copy return arguments */
    if (node != NULL) {
      *node = chn->chGroup;
    }
    if (tp != NULL) {
      *tp = chn->chNum;
    }
    //	 printf("tpIsValid node=%d tp=%d ifo=%d tpNum=%d\n",
    //                 chn->rmId, chn->chNum, chn->ifoId, chn->tpNum);
    return 1;
  } else {
    return 0;
  }
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: tpType					*/
/*                                                         		*/
/* Procedure Description: returns the test point type			*/
/*                                                         		*/
/* Procedure Arguments: channel info					*/
/*                                                         		*/
/* Procedure Returns: type of test point				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
testpointtype testpoint_type(const gdsChnInfo_t *chn) {
#ifdef _NO_TESTPOINTS
  return tpInvalid;
#else
  testpoint_t tp; /* test point id */

  if (chn == NULL) {
    return tpInvalid;
  }

  /* valid test point? */
  if (!testpoint_is_valid(chn, NULL, &tp)) {
    return tpInvalid;
  }

  /* check tp id */
  switch (TP_ID_TO_INTERFACE(tp)) {
  /* LSC excitation test point channel */
  case TP_EX_INTERFACE:
    return tpLSCExc;
  /* LSC test point channel */
  case TP_TP_INTERFACE:
    return tpLSC;
  /* DAC channel */
  case TP_DAC_INTERFACE:
    return tpDAC;
    /* DS340 channel */
  case TP_DS340_INTERFACE:
    return tpDSG;
    /* not an excitation test point/channel */
  default:
    return tpInvalid;
  }
#endif
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: tpReadback					*/
/*                                                         		*/
/* Procedure Description: returns the readback channel of a test point	*/
/*                                                         		*/
/* Procedure Arguments: channel info, readback channel info (return)	*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 otherwise			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int test_point_readback(const gdsChnInfo_t *chn, gdsChnInfo_t *rb) {
#ifdef _NO_TESTPOINTS
  return -10;
#else

  if ((chn == NULL) || (rb == NULL)) {
    return -1;
  }

  /* test if DAC or DS340 channel */
  /*if (tpType (chn) == tpDAC) {
     return -2;
  }
  else */
  if (testpoint_type(chn) == tpDSG) {
    return -2;
  } else {
    /* otherwise assume it is the same */
    memcpy(rb, chn, sizeof(gdsChnInfo_t));
  }
  return 0;

#endif
}
