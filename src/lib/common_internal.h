//
// Created by erik.vonreis on 9/27/23.
//

#ifndef LIBTESTPOINT_COMMON_INTERNAL_H
#define LIBTESTPOINT_COMMON_INTERNAL_H

/** Default program number for the test point server. The default
    program number is 0x31001001.

    @author DS, August 98
    @see Remote Procedure Call Interface
************************************************************************/
#define RPC_PROGNUM_TESTPOINT 0x31002000

/** Default version number for the test point server. The default
    version number is 1.

    @author DS, August 98
    @see Remote Procedure Call Interface
************************************************************************/
#define RPC_PROGVER_TESTPOINT 1

#endif // LIBTESTPOINT_COMMON_INTERNAL_H
