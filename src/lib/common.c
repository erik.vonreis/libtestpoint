//
// Created by erik.vonreis on 9/27/23.
//

//
// Created by erik.vonreis on 9/13/23.
//
#include <stdio.h>

#ifndef LIBTESTPOINT_VERSION
#error LIBTESTPOINT_VERSION must be defined
#endif

/**
 * Network interface versions
 *
 * 1 - broadcast plain string protocol inherited from ALIGO.
 *
 */
#define NETWORK_INTERFACE_VERSION 1

#include "stdlib.h"

static int version_compare(int maj0, int min0, int rel0, int maj1, int min1,
                           int rel1) {
  if (maj0 < maj1) {
    return -1;
  }
  if (maj0 > maj1) {
    return 1;
  }
  if (min0 < min1) {
    return -1;
  }
  if (min0 > min1) {
    return 1;
  }
  if (rel0 < rel1) {
    return -1;
  }
  if (rel0 > rel1) {
    return 1;
  }
  return 0;
}

void testpoint_version(int *major_version, int *minor_version,
                       int *release_version) {
  sscanf(LIBTESTPOINT_VERSION, "%d.%d.%d", major_version, minor_version,
         release_version);
}

const char *testpoint_is_compatible_with(int major_version, int minor_version,
                                         int release_version) {
  int maj_ver, min_ver, rel_ver;

  testpoint_version(&maj_ver, &min_ver, &rel_ver);

  int comp = version_compare(major_version, minor_version, release_version,
                             maj_ver, min_ver, rel_ver);
  if (comp <= 0) {
    return NULL;
  }
  return "Requested version is newer than the library version.";
}

int testpoint_network_interface_version() { return NETWORK_INTERFACE_VERSION; }