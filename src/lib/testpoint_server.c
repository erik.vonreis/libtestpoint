/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: testpoint_server					*/
/*                                                         		*/
/* Module Description: implements server functions for handling the 	*/
/* test point interface							*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* Header File List: */
#ifndef __EXTENSIONS__
#define __EXTENSIONS__
#endif
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>

#include <glib.h>

#include <cds/cdsutil.h>

#include <gdsconf/gdsconfserver.h>

#include "rtestpoint.h"
#include "testpoint_info.h"
#include "testpoint_interface.h"
#include "testpoint_server.h"
#include "testpoint_server_internal.h"

#include "daq_core.h"

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Constants: _KEEPALIVE_TIMEOUT  timeout for keep alive signal		*/
/* 	      _KEEP_AROUND	  time of keeping an expired client	*/
/* 	      _CHNNAME_SIZE	  size of channel name			*/
/* 	      _TP_MAX_USER	  max. number of clients/users		*/
/* 	      _MAX_TPNAMES	  max. number of tp names in one call	*/
/* 	      PRM_FILE		  parameter file name			*/
/*            PRM_SECTION	  parameter section name		*/
/*            PRM_ENTRY1	  entry name for host 			*/
/*            PRM_ENTRY2	  entry name for rpc prog num		*/
/*            PRM_ENTRY3	  entry name for rpc ver num		*/
/*            PRM_ENTRY4	  entry name for control system name	*/
/*            _NODE0_LSCX_UNIT_ID unit id of 4K LSC excitation		*/
/*            _NODE0_ASCX_UNIT_ID unit id of 4K ASC excitation		*/
/*            _NODE1_LSCX_UNIT_ID unit id of 2K LSC excitation		*/
/*            _NODE1_ASCX_UNIT_ID unit id of 2K ASC excitation		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#define _KEEPALIVE_TIMEOUT_S 60
#define _KEEP_AROUND_S 240
#define _CHNNAME_SIZE MAX_CHNNAME_SIZE
#define _TP_MAX_USER 1000
#define _MAX_TPNAMES 1000
#define TP_MAX_PRESELECT 20
#define PRM_SECTION gdsSectionSite("node%i")
#define PRM_PRESELECT "preselect%i"
#define PRM_ENTRY1 "hostname"
#define PRM_ENTRY2 "prognum"
#define PRM_ENTRY3 "progver"
#define PRM_ENTRY4 "system"

#define _SVC_MODE 0 // single threaded server
#define _SVC_FG 1   // run in foreground

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Types: tpClient_t		client entry (keep alive)		*/
/* 	  tpUsage_t		client (user) id			*/
/* 	  tpEntry_t		internal rep. of a test point		*/
/*        tpNode_t		test point node				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

struct tpClient_t {
  /* valid */
  int valid;
  /* time of last contact */
  gps_ns_t lastTime;
};
typedef struct tpClient_t tpClient_t;

struct tpUsage_t {
  /* time of request */
  gps_ns_t reqTime;
  /* timeout */
  gps_ns_t timeout;
  /* client id */
  int clntId;
};
typedef struct tpUsage_t tpUsage_t;

struct tpEntry_t {
  /* testpoint ID */
  testpoint_t id;
  /* in use count */
  int inUse;
  /* user identification array */
  tpUsage_t users[_TP_MAX_USER];
  /* channel name of testpint */
  char name[_CHNNAME_SIZE];
};
typedef struct tpEntry_t tpEntry_t;

struct tpNode_t {
  /* true if node is valid */
  int valid;
  /* Node number of the node. */
  int node;
  /* rpc hostname */
  char hostname[80];
  /* rpc prog num */
  unsigned long prognum;
  /* rpc prog ver. */
  unsigned long progver;
  /* list of preselected testpoints */
  testpoint_t preselect[TP_MAX_PRESELECT];
  /* list of TP active TPs */
  tpEntry_t indx[TP_MAX_INTERFACE][DAQ_GDS_MAX_TP_NUM];
  /* list of testpoint channel information */
  gdsChnInfo_t *tplist;
  /* length of list */
  int tplistlen;
  /* fast lookup table: tp ID -> channel info */
  gdsChnInfo_t *tplookup[TP_HIGHEST_INDEX];
};
typedef struct tpNode_t tpNode_t;

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Globals: sd			scheduler				*/
/*	    servermux		protects globals			*/
/*          initServer		if 0 the server is not yet initialized	*/
/*          shutdownflag	shutdown flag				*/
/*          tplist		list of test points			*/
/*          tpclnts	        client list with keep alive   		*/
/*          tpclntID	        client ID for clients w/o keep alive	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

// locks out internal data structures
G_LOCK_DEFINE_STATIC(servermux);

static int initServer = 0;
static int shutdownflag = 0;
static tpNode_t tpNode;
static tpClient_t tpclnts[_TP_MAX_USER];
static int lowest_invalid_client = 0;
static int tpclntID = _TP_MAX_USER + 1;

static int model_rate_Hz = 0;

static testpoint_reader_callback read_testpoints = NULL;

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Forward declarations: 						*/
/*	finiTestpointServer	termionates testpoint server		*/
/*	tpName2Index		translate tp names into id numbers	*/
/*	finiTestpointServer	cleans up testpoint server		*/
/*	initializeTestpoints	sets up testpoint server		*/
/*	cleanupTestpoints	garbage collector for test points	*/
/*	updateTestpoints	updates testpoints in the refl. mem.	*/
/*      rtestpoint_1		rpc dispatch function			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

// static void setPreselect(int node);
// static int tpName2Index(int node, const char *tpNames, TP_r *tp);
// static int initializeTestpoints(void);
// static int cleanupTestpoints();
extern void rtestpoint_1(struct svc_req *rqstp, register SVCXPRT *transp);
static int curDaqBlockSize;

static GDSCONF_SERVER_HANDLE conf_server = NULL;

// this data block is updated by the client
// only the client thread should write to it.
testpoint_block active_block;

// this points to a data block that represents
// the latest testpoint block available to the
// server application
// it is to be stored to and loaded from atomically.
// only the client thread should store to it.
// When the server application loads the pointer,
// it can dispose of the memory for the block by
// calling testpoint_free_block()
testpoint_block *latest_block = NULL;

// lock access to latest_block
G_LOCK_DEFINE_STATIC(latest_block);
// set when latest_block is updated with a new block
GCond latest_updated;

/**
 * Make a copy of the active_block, set the index of the copy
 * Post the copy atomically as latest_block.
 * Ownership of the new block belongs to whoever pops it from
 * latest_block, and it should be freed with testpoint_free_block()
 * @return 0 if the block cannot be allocated.  1 otherwise.
 */
static int post_active_block() {
  testpoint_block *copy_block =
      (testpoint_block *)malloc(sizeof(testpoint_block));
  if (copy_block == NULL) {
    return 0;
  }
  memcpy(copy_block, &active_block, sizeof(testpoint_block));

  testpoint_block *last = NULL;

  G_LOCK(latest_block);
  last = latest_block;
  latest_block = copy_block;
  g_cond_signal(&latest_updated);
  G_UNLOCK(latest_block);

  // the previous block was never popped by the server.
  if (last != NULL) {
    testpoint_free_block(last);
  }
}

testpoint_block *testpoint_get_latest_block(int timeout_s) {
  testpoint_block *last;
  G_LOCK(latest_block);
  last = latest_block;
  latest_block = NULL;
  G_UNLOCK(latest_block);
  return last;
}

void testpoint_free_block(testpoint_block *block) { free(block); }

/* -------------------------------- */
/* find an unused client id struct  */
/* mark it as used and return the   */
/* index to it.                     */
/* -------------------------------- */
int allocate_client_id() {
  G_LOCK(servermux);
  int ret = -1;
  if (lowest_invalid_client < _TP_MAX_USER) {
    ret = lowest_invalid_client;
    tpclnts[ret].valid = 1;
    tpclnts[ret].lastTime = GPSnow_ns();
    int found = 0;
    for (int i = lowest_invalid_client; i < _TP_MAX_USER; i++) {
      /* look for free id */
      if (!tpclnts[i].valid) {
        lowest_invalid_client = i;
        found = 1;
        break;
      }
    }
    if (!found) {
      lowest_invalid_client = _TP_MAX_USER;
    }
  }
  G_UNLOCK(servermux);
  return ret;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: setPreselect				*/
/*                                                         		*/
/* Procedure Description: set preselected TPs				*/
/*                                                         		*/
/* Procedure Arguments: node						*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static void setPreselect(int node) {
  resultRequestTP_r result; /* result from request */
  TP_r tp;                  /* TP list */
  int k;                    /* index */

  /* initialize TP list */
  tp.TP_r_len = 0;
  tp.TP_r_val = malloc(2 * TP_MAX_PRESELECT * sizeof(short));
  if (tp.TP_r_val == NULL) /* JCB */
  {
    gdsDebug("setPreselect malloc failed.");
    return;
  }
  /* select TPs */
  for (k = 0; k < TP_MAX_PRESELECT; ++k) {
    if (tpNode.preselect[k] > 0) {
      tp.TP_r_val[tp.TP_r_len++] = tpNode.preselect[k];
    }
  }
  if (tp.TP_r_len > 0) {
    requesttp_1_svc(0x7fffffff, node, tp, 0, &result, 0);
  }
  free(tp.TP_r_val);
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: tpName2Index				*/
/*                                                         		*/
/* Procedure Description: translates test point names into id numbers	*/
/*                                                         		*/
/* Procedure Arguments: node, test point names, test point list		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if not (status)		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static int tpName2Index(int node, const char *tpNames, TP_r *tp) {
  char *buf;        /* name buffer */
  char *p;          /* cursor into buffer */
  gdsChnInfo_t chn; /* channel info structure */
  int tp_node;      /* node of test point name */
  char *saveptr;
  testpoint_t *testpoints;

  /* allocate memory */
  buf = malloc(strlen(tpNames) + 2);
  if (NULL == buf) /* JCB */
  {
    gdsDebug("tpName2Index malloc(strlen(tpNames)+2) failed.");
    return -1;
  }
  tp->TP_r_val = malloc(_MAX_TPNAMES * sizeof tp->TP_r_val[0]);
  if (tp->TP_r_val == NULL) {
    gdsDebug("tpName2Index malloc(_MAX_TPNAMES...) [1] failed"); /* JCB */
    free(buf);
    return -1;
  }
  testpoints = malloc(_MAX_TPNAMES * sizeof(testpoint_t));
  if (testpoints == NULL) {
    gdsDebug("tpName2Index malloc(_MAX_TPNAMES...) [2] failed"); /* JCB */
    free(buf);
    free(tp->TP_r_val);
    return -1;
  }
  tp->TP_r_len = 0;
  strcpy(buf, tpNames);

  /* go through name list */
  p = strtok_r(buf, " ,\t\n", &saveptr);
  while ((p != NULL) && (tp->TP_r_len < _MAX_TPNAMES)) {
    printf("tpName2Index checking %s\n", p);
    if ((gdsChannelInfo(p, &chn) == 0) &&
        (testpoint_is_valid(&chn, &tp_node, testpoints + tp->TP_r_len)) &&
        (node == tp_node)) {
      printf("%s is tp %i (node %i)\n", p, testpoints[tp->TP_r_len], tp_node);
      tp->TP_r_len++;
    }
    printf("node=%d; tp_node=%d\n", node, tp_node);
    p = strtok_r(NULL, " \t\n", &saveptr);
  }

  /* copy back into rpc structure */
  for (int i = 0; i < tp->TP_r_len; ++i) {
    tp->TP_r_val[i] = testpoints[i];
  }

  free(testpoints);
  free(buf);
  return 0;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: testpoint_cleanup				*/
/*                                                         		*/
/* Procedure Description: garbage collector for test points		*/
/*                                                         		*/
/* Procedure Arguments: void		*/
/*                                                         		*/
/* Procedure Returns: 1 if successful, 0 if not			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int testpoint_server_cleanup(void) {
  int i;          /* index into test point list */
  int j;          /* interface ID */
  int k;          /* index into usage array */
  int len;        /* length of test point list */
  tpEntry_t *tpp; /* test point entry */
  gps_ns_t t;     /* current time */
  int remove;     /* true if tp is obsolete */

  t = GPSnow_ns();

  if (tpNode.valid) {

    /* get server mutex */

    /* loop over interfaces */
    for (j = 0; j < TP_MAX_INTERFACE; j++) {
      int changed = 0;
      G_LOCK(servermux);
      /* loop over selected test points */
      len = DAQ_GDS_MAX_TP_NUM;
      for (i = 0; i < len; i++) {
        tpp = &tpNode.indx[j][i];
        if (tpp->id == 0) {
          continue;
        }
        /* loop over clients */
        for (k = 0; k < tpp->inUse; k++) {
          remove = 0;
          /* check time out */
          if ((tpp->users[k].timeout > 0) &&
              (tpp->users[k].reqTime + tpp->users[k].timeout < t)) {
            /* found old testpoint */
            /* printf ("remove tp %i (node %i) because of timeout\n",
                   tpp->id, tpNode.node);*/
            remove = 1;
          }

          /* remove entry if obsolete */
          if (remove) {
            /* decrease in use count */
            tpp->inUse--;
            if (tpp->inUse <= 0) {
              tpp->id = 0;
            }
            /* shift array */
            if (k < tpp->inUse) {
              memmove(tpp->users + k, tpp->users + k + 1,
                      (tpp->inUse - k) * sizeof(tpUsage_t));
            }
            update_active_testpoints(j, i, 0);
            changed = 1;
          }
        }
      }

      if (changed) {
        post_active_block();
      }

      /* release server mutex */
      G_UNLOCK(servermux);
    }
  }

  /* loop over client interface */
  for (k = 0; k < _TP_MAX_USER; k++) {
    /* if expired keep it around for a while than delete it */
    if ((tpclnts[k].valid) &&
        (tpclnts[k].lastTime + _KEEPALIVE_TIMEOUT * _ONESEC_NS < t)) {
      if (++tpclnts[k].valid > _KEEP_AROUND) {
        tpclnts[k].valid = 0;
        if (k < lowest_invalid_client) {
          lowest_invalid_client = k;
        }
      }
    } else if (tpclnts[k].valid > 1) {
      // valid is used as a secondary timeout counter.
      // reset if we aren't timed out
      tpclnts[k].valid = 1;
    }
  }

  return 0;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: initializeTestpoints			*/
/*                                                         		*/
/* Procedure Description: initializes server stuff			*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static int initializeTestpoints(void) {

  /* quit if already initiaized */
  if (initServer == 2) {
    return 0;
  }

  /* reset test point data memory */
  memset(&tpNode, 0, sizeof(tpNode));
  memset(tpclnts, 0, sizeof(tpclnts));

  /* set initServer and return */
  shutdownflag = 1;
  initServer = 1;

  initServer = 2;
  return 0;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: isTestpoint					*/
/*                                                         		*/
/* Procedure Description: returns true if channel is a test point	*/
/*                                                         		*/
/* Procedure Arguments: channel info					*/
/*                                                         		*/
/* Procedure Returns: true if yes, false if no				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static int _query_node = 0; /* rm ID of query */
static int isTestpoint(const gdsChnInfo_t *info) {
  return IS_TP(info) && (info->rmId == _query_node);
}

/**
 * Get the node numer for system_name from the param file.
 * @param system_name
 * @param prm_path
 * @return node number, or zero on failure, also fills in "hostname" on success
 */
static int find_node_number(const char *system_name, const char *prm_path,
                            char *hostname) {
  char fname[1024];
  snprintf(fname, sizeof(fname), "%s/%s", prm_path, "testpoint.par");
  char section_name[PARAM_ENTRY_LEN];

  FILE *f = fopen(fname, "rt");
  if (NULL == f) {
    return 0;
  }

  while (NULL != nextParamFileSection(f, section_name)) {
    int num_entries;
    char *section = getParamFileSection(f, NULL, &num_entries, 0);

    int cursor = -1;
    char value[PARAM_ENTRY_LEN];
    if (loadParamSectionEntry("system", section, num_entries, &cursor, 3,
                              value)) {
      continue;
    }
    if (!strcmp(value, system_name)) {
      char ifo_prefix;
      int node;
      if (2 == sscanf(section, "%c-node%d", &ifo_prefix, &node)) {

        cursor = -1;
        if (NULL != hostname) {
          loadParamSectionEntry("hostname", section, num_entries, &cursor, 3,
                                hostname);
        }

        return node;
      } else {
        fprintf(stderr, "bad section %s when reading testpoint.par file\n",
                section);
      }
    }
  }
  return 0;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: testpoint_server				*/
/*                                                         		*/
/* Procedure Description: start rpc service task for test points	*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: does not return if successful, 0 otherwise	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int testpoint_start_server(const char *system_name, const char *prm_path,
                           testpoint_reader_callback reader_callback) {
  int j;           /* tp interface index */
  SVCXPRT *transp; /* service transport */
  int proto;       /* protocol */
  char remotehost[PARAM_ENTRY_LEN];
  /* remote hostname */
  char section[30];      /* section name */
  char param[30];        /* paramter name */
  struct in_addr addr;   /* host address */
  struct in_addr laddr;  /* local address */
  unsigned long prognum; /* rpc prog. num. */
  unsigned long progver; /* rpc prog. ver. */
  unsigned long presel;  /* preselected tp */
  int node;              /* node id */
  int n;                 /* node id */
  int k;                 /* index */
  int duplicate;         /* duplicate prog num/ver */
  int reg;               /* # of registered services */
  int tp;                /* tp index */

  /* configuration service */
  struct in_addr host; /* local host address */

  /* initialize test points */
  if (initializeTestpoints() < 0) {
    fprintf(stderr, "unable to initialize test points\n");
    gdsError(GDS_ERR_PROG, "unable to initialize test points");

    return 0;
  }

  if (NULL == reader_callback) {
    gdsError(GDS_ERR_PROG, "reader_callback cannot be NULL");
  }
  read_testpoints = reader_callback;

  // initialize active block with current testpoints
  if (!read_testpoints(&active_block)) {
    fprintf(stderr, "Couldn't read testpoint data from host application\n");
    return 0;
  }

  /* Mark the node as invalid until it's found in the parameter file. */
  tpNode.valid = 0;

  int rate_Hz;
  int result = get_model_rate_dcuid(&rate_Hz, &node, system_name, prm_path);
  if (result) {
    fprintf(stderr, "error getting model rate: %d\n", result);
    gdsError(GDS_ERR_PROG, "Unable to find model testpoint file");
    return 0;
  }

  model_rate_Hz = rate_Hz;

  if (node == 0) {
    gdsError(GDS_ERR_PROG,
             "couldn't find node number that matches system name");
    return 0;
  }

  prognum = RPC_PROGNUM_TESTPOINT;
  progver = RPC_PROGVER_TESTPOINT;

  // Use GDS node to generate a unique RPC number
  prognum += node;

  tpNode.prognum = prognum;
  tpNode.progver = progver;

  memset(tpNode.indx, 0,
         TP_MAX_INTERFACE * DAQ_GDS_MAX_TP_NUM * sizeof(tpEntry_t));

  /* Mark the node as valid and record the node number. */
  tpNode.valid = 1;
  tpNode.node = node;

  char param_file[1024];
  sprintf(param_file, "%s/testpoint.par", prm_path);

  /* get preselected testpoints */
  for (k = 0; k < TP_MAX_PRESELECT; ++k) {
    sprintf(param, PRM_PRESELECT, k);
    presel = 0;
    loadNumParam(param_file, section, param, &presel);
    if ((presel > 0) && (presel < TP_HIGHEST_INDEX)) {
      tpNode.preselect[k] = presel;
      printf("Preselecting testpoint %ld on node %d\n", presel, node);
    } else {
      tpNode.preselect[k] = 0;
    }
  }

  /* load channel information */
  if (!tpNode.valid) {
    printf("Failed to find my node in %s\n", param_file);
    exit(1);
  } else {
    /* query length of channel info list of the specified node */
    _query_node = node;
    char tp_file[1024];
    snprintf(tp_file, sizeof(tp_file), "%s/tpchn_%s.par", prm_path,
             system_name);
    channel_client(tp_file);
    tpNode.tplistlen = gdsChannelListLen(-1, isTestpoint);
    printf("Channel list length for node %d is %d\n", node, tpNode.tplistlen);
    if (tpNode.tplistlen <= 0) {
      tpNode.tplistlen = 0;
    } else {
      /* allocate memory for list */
      tpNode.tplist = malloc(tpNode.tplistlen * sizeof(gdsChnInfo_t));
      if (tpNode.tplist == NULL) {
        tpNode.tplistlen = 0;
        return 0;
      }
      /* load list */
      tpNode.tplistlen =
          gdsChannelList(-1, isTestpoint, tpNode.tplist, tpNode.tplistlen);
      if (tpNode.tplistlen <= 0) {
        free(tpNode.tplist);
        tpNode.tplistlen = 0;
        tpNode.tplist = NULL;
      } else {
        /* build lookup table */
        memset(tpNode.tplookup, 0, sizeof(tpNode.tplookup));
        for (j = 0; j < tpNode.tplistlen; j++) {
          tp = tpNode.tplist[j].chNum;
          if ((tp > 0) && (tp < TP_HIGHEST_INDEX)) {
            tpNode.tplookup[tp] = tpNode.tplist + j;
          }
        }
      }
    }
  }

  /* clear memory */
  /* loop through interfaces */

  memset((void *)&active_block, 0, sizeof(active_block));

  // safe to post here because we haven't started the server yet
  // the post is a convenience for servers that want
  // to initialize with a zeroed block
  post_active_block();

  if (tpNode.valid) {
    setPreselect(tpNode.node);
  }

  /* get local address */
  if (rpcGetLocaladdress(&host) < 0) {
    gdsError(GDS_ERR_PROG, "unable to obtain local address");
    return 0;
  }

  /* init rpc services */
  if (rpcInitializeServer(_SVC_FG, _SVC_MODE, &transp, &proto) < 0) {
    gdsError(GDS_ERR_PROG, "unable to start rpc service");
    return 0;
  }

  /* register with rpc service */
  if (rpcRegisterService(transp, proto, tpNode.prognum, tpNode.progver,
                         rtestpoint_1) != 0) {
    gdsError(GDS_ERR_PROG, "unable to register test point service");
    return 9;
  }
  printf("Test point manager (%lx / %li): node %i\n", tpNode.prognum,
         tpNode.progver, node);

  gdsconf_confinfo_t conf_info;
  gdsconf_confinfo_setup(&conf_info, CONFIG_SERVICE_TP, node, tpNode.prognum,
                         tpNode.progver);
  conf_server = gdsconf_server_start(&conf_info, 1);
  if (NULL == conf_server) {
    gdsWarningMessage("unable to start configuration services");
  }

  /* start server */
  rpcStartServer();

  /* never reached */
  return 1;
}

/**
 *
 * @return the server node (DCUID)
 */
int testpoint_get_node() { return tpNode.node; }

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: finiTestpointServer				*/
/*                                                         		*/
/* Procedure Description: cleans up test point server			*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns:void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
void testpoint_stop_server(void) {
  int node; /* node ID */

  if (initServer == 0) {
    return;
  }

  /* free memory */
  if (tpNode.valid && (tpNode.tplist != NULL)) {
    free(tpNode.tplist);
  }

  if (conf_server != NULL) {
    gdsconf_server_stop(conf_server);
  }

  /* set initServer and return */
  shutdownflag = 0;
  initServer = 0;
}

int update_active_testpoints(int interface, int slot, testpoint_t tp) {
  testpoint_t *testpoints;
  switch (interface) {
  case TP_EX_INTERFACE:
    testpoints = active_block.excitations;
    break;
  case TP_TP_INTERFACE:
    testpoints = active_block.testpoints;
    break;
  default:
    fprintf(stderr, "ERROR: Unknown interface %d in update_active_testpoints\n",
            interface);
    return 0;
  }
  if (slot < 0) {
    fprintf(
        stderr,
        "ERROR: slot value must not be negative in update_active_testpoints\n");
    return 0;
  }
  if (slot >=
      sizeof(active_block.excitations) / sizeof(active_block.excitations[0])) {
    fprintf(stderr,
            "ERROR: testpoint slot number %d is too large. There are only %d "
            "slots.\n",
            slot,
            sizeof(active_block.excitations) /
                sizeof(active_block.excitations[0]));
    return 0;
  }
  testpoints[slot] = tp;

  return 1;
}

/**
 * RPC call handlers
 */

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Remote Procedure Name: requesttp_1_svc				*/
/*                                                         		*/
/* Procedure Description: sets test points				*/
/*                                                         		*/
/* Procedure Arguments: node ID, test point list, timeout		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if not (status)		*/
/*                    time and epoch of activation         		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
bool_t requesttp_1_svc(int id, int node, TP_r tp, quad_t timeout,
                       resultRequestTP_r *result, struct svc_req *rqstp) {

  int i, j, k;     /* index into test point list */
  int tpinterface; /* tp interface id */
  int len;         /* length of test point list */
  tpEntry_t *tpp;  /* test point entry */
  gps_ns_t time;   /* activation time */

  gdsDebug("request test point");

  /* test node ID */
  if ((id < 0) || (node < 0) || (node >= TP_MAX_NODE) || !tpNode.valid ||
      tpNode.node != node) {
    result->status = -2;
    return TRUE;
  }

  /* get server mutex */
  G_LOCK(servermux);

  /* go through supplied test point list */
  result->status = 0;

  // Loop through the list of testpoint to set and check for DAQ overload
  int projTpNum = 0; /* Projected test point number */
  for (i = 0; i < tp.TP_r_len; i++) {
    /* check if ID in list */
    if ((tp.TP_r_val[i] <= 0) || (tp.TP_r_val[i] >= TP_HIGHEST_INDEX) ||
        (tpNode.tplookup[tp.TP_r_val[i]] == NULL)) {
      continue;
    }

    /* test whether valid test point */
    tpinterface = TP_ID_TO_INTERFACE(tp.TP_r_val[i]);
    /* use LSC EXC for DAC channels */
    if (tpinterface == TP_DAC_INTERFACE) {
      tpinterface = TP_EX_INTERFACE;
    }
    len = DAQ_GDS_MAX_TP_NUM;
    if ((tpinterface < 0) || (tpinterface >= TP_MAX_INTERFACE)) {
      continue;
    }

    /* test whether already in list */
    for (j = 0, k = -1; j < len; j++) {
      tpp = &tpNode.indx[tpinterface][j];
      /* check for duplicate */
      if (tpp->id == tp.TP_r_val[i]) {
        /* already in list; check in use */
        if (tpp->inUse < _TP_MAX_USER) {
          tpp->users[tpp->inUse].reqTime = GPSnow_ns();
          tpp->users[tpp->inUse].timeout = timeout;
          tpp->users[tpp->inUse].clntId = id;
          //         tpp->inUse++;
          k = -2;
          break;
        }
        /* too many users */
        else {
          k = -1;
          break;
        }
      }
      if ((k == -1) && (tpp->id == 0)) {
        /* found an empty slot */
        k = j;
        projTpNum++;
      }
    }
  }
  /* Check if the total number of testpoints has reached the limit */
  {
    int tpiface;
    int projTpNumNewEntry = -1;
    for (tpiface = 0; tpiface < TP_MAX_INTERFACE; tpiface++) {
      /* loop over selected test points */
      int idxlen = DAQ_GDS_MAX_TP_NUM;
      int tpidx;
      for (tpidx = 0; tpidx < idxlen; tpidx++) {
        tpEntry_t *tpent; /* test point entry */
        tpent = &tpNode.indx[tpiface][tpidx];
        if (tpent->id != 0)
          projTpNum++;
      }
    }

    unsigned int projDaqSize;

    /**
     * If model rate is known, use that.
     *
     * Models slower than 16kHz will be allowed more test points.
     */
    if (model_rate_Hz) {
      projDaqSize = projTpNum * 4 * model_rate_Hz + curDaqBlockSize;
    } else {
      projDaqSize = projTpNum * sizeof(float) * model_rate_Hz + curDaqBlockSize;
    }
    if (projDaqSize > DAQ_DCU_SIZE) {
      printf("Too many testpoints: projected DAQ size %d > %d (maximum)\n",
             projDaqSize, DAQ_DCU_SIZE);
      k = -1;
      G_UNLOCK(servermux);
      return TRUE;
    }
  }

  for (i = 0; i < tp.TP_r_len; i++) {
    /* debug */
    /* check if ID in list */
    if ((tp.TP_r_val[i] <= 0) || (tp.TP_r_val[i] >= TP_HIGHEST_INDEX) ||
        (tpNode.tplookup[tp.TP_r_val[i]] == NULL)) {
      continue;
    }

    /* test whether valid test point */
    tpinterface = TP_ID_TO_INTERFACE(tp.TP_r_val[i]);
    /* use LSC EXC for DAC channels */
    if (tpinterface == TP_DAC_INTERFACE) {
      tpinterface = TP_EX_INTERFACE;
    }
    len = DAQ_GDS_MAX_TP_NUM;
    if ((tpinterface < 0) || (tpinterface >= TP_MAX_INTERFACE)) {
      continue;
    }

    /* test whether already in list */
    for (j = 0, k = -1; j < len; j++) {
      tpp = &tpNode.indx[tpinterface][j];
      /* check for duplicate */
      if (tpp->id == tp.TP_r_val[i]) {
        /* already in list; check in use */
        if (tpp->inUse < _TP_MAX_USER) {
          tpp->users[tpp->inUse].reqTime = GPSnow_ns();
          tpp->users[tpp->inUse].timeout = timeout;
          tpp->users[tpp->inUse].clntId = id;
          tpp->inUse++;
          k = -2;
          break;
        }
        /* too many users */
        else {
          k = -1;
          break;
        }
      }
      if ((k == -1) && (tpp->id == 0)) {
        /* found an empty slot */
        k = j;
      }
    }

    /* allocate a new test point */
    if (k >= 0) {

      /* debug */
      tpp = &tpNode.indx[tpinterface][k];
      tpp->id = tp.TP_r_val[i];
      strncpy(tpp->name, tpNode.tplookup[tp.TP_r_val[i]]->chName,
              _CHNNAME_SIZE);
      tpp->name[_CHNNAME_SIZE - 1] = 0;
      tpp->inUse = 0;
      tpp->users[tpp->inUse].reqTime = GPSnow_ns();
      tpp->users[tpp->inUse].timeout = timeout;
      tpp->users[tpp->inUse].clntId = id;
      tpp->inUse++;
      update_active_testpoints(tpinterface, k, tp.TP_r_val[i]);
    } else if (k == -1) {
      /* no empty slot or too many users */
      result->status = -(i + 1);
      break;
    }
  }

  post_active_block();

  /* if failed, cleanup */
  if (result->status < 0) {
    int ret;
    TP_r tpcln;
    tpcln.TP_r_len = -result->status;
    tpcln.TP_r_val = malloc(tpcln.TP_r_len * sizeof(tpcln.TP_r_val[0]));
    if (tpcln.TP_r_val != 0) {
      for (i = 0; i < tpcln.TP_r_len; i++) {
        tpcln.TP_r_val[i] = tp.TP_r_val[i];
      }
      cleartp_1_svc(id, node, tpcln, &ret, rqstp);
    } else /* JCB */
    {
      gdsDebug("requesttp_1_svc malloc (tpcln.TP_r_len...) failed.");
      return FALSE;
    }
  }

  /* release server mutex */
  G_UNLOCK(servermux);
  result->time = 0;
  result->epoch = 0;
  return TRUE;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Remote Procedure Name: requesttpname_1_svc				*/
/*                                                         		*/
/* Procedure Description: sets test points				*/
/*                                                         		*/
/* Procedure Arguments: test point names, timeout			*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if not (status)		*/
/*                    time and epoch of activation         		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
bool_t requesttpname_1_svc(int id, char *tpNames, quad_t timeout,
                           resultRequestTP_r *result, struct svc_req *rqstp) {
#ifdef _NO_TESTPOINTS
  result->status = -10;
  return TRUE;

#else
  int node; /* node number */
  TP_r tp;  /* list of test points */

  gdsDebug("request test point by name");
  printf("Request tp by name %s\n", tpNames); /* JCB */

  if (tpNode.valid) {
#ifdef DEBUG
    printf("tp name (node %i) = %s\n", tpNode.node, tpNames);
#endif

    /* translate names into test points */
    if (tpName2Index(tpNode.node, tpNames, &tp) < 0) {
      result->status = -1;
      return TRUE;
    }
    /* request test points */
    requesttp_1_svc(id, tpNode.node, tp, timeout, result, rqstp);
    free(tp.TP_r_val);
    if (result->status < 0) {
      result->status = -2;
      return TRUE;
    }
  }

  return TRUE;
#endif
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Remote Procedure Name: clearnametp_1_svc				*/
/*                                                         		*/
/* Procedure Description: clears test points by name			*/
/*                                                         		*/
/* Procedure Arguments: test point names				*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if not			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
bool_t cleartpname_1_svc(int id, char *tpNames, int *result,
                         struct svc_req *rqstp) {

  int node; /* node number */
  TP_r tp;  /* list of test points */

  gdsDebug("clear test point by name");
  printf("Clear test point by name %s\n", tpNames); /* JCB */

  /* translate names into test points */
  if (tpName2Index(tpNode.node, tpNames, &tp) >= 0) {
    /* clear test points */
    cleartp_1_svc(id, tpNode.node, tp, result, rqstp);
    free(tp.TP_r_val);
  }

  *result = 0;
  return TRUE;
}

bool_t cleartp_1_svc(int id, int node, TP_r tp, int *result,
                     struct svc_req *rqstp) {
  int i, j;        /* index into test point list */
  int k;           /* index into user list */
  int tpinterface; /* tp interface id */
  int len;         /* length of test point list */
  tpEntry_t *tpp;  /* test point entry */
  int clearall;    /* clear all */

  gdsDebug("clear test point");

  /* test node ID */
  if ((id < 0) || (node < 0) || (node >= TP_MAX_NODE) || !tpNode.valid ||
      tpNode.node != node) {
    *result = -2;
    return TRUE;
  }

  /* get server mutex */
  G_LOCK(servermux);
  clearall = 0;

  /* go through supplied test point list */
  for (i = 0; i < tp.TP_r_len; i++) {
    /* debug */

    if (tp.TP_r_val[i] == _TP_CLEAR_ALL) {
      /* clear all test points of this node */
      memset(tpNode.indx, 0,
             TP_MAX_INTERFACE * DAQ_GDS_MAX_TP_NUM * sizeof(tpEntry_t));
      memset(&active_block, 0, sizeof(active_block));
      clearall = 1;
      break;
    }
    /* else clear only specific test points */
    tpinterface = TP_ID_TO_INTERFACE(tp.TP_r_val[i]);
    /* use LSC EXC for DAC channels */
    if (tpinterface == TP_DAC_INTERFACE) {
      tpinterface = TP_EX_INTERFACE;
    }
    len = DAQ_GDS_MAX_TP_NUM;
    if ((tpinterface < 0) || (tpinterface >= TP_MAX_INTERFACE)) {
      continue;
    }

    /* search through list */
    for (j = 0; j < len; j++) {
      tpp = &tpNode.indx[tpinterface][j];
      if (tpp->id == tp.TP_r_val[i]) {
        /* found test point */
        for (k = 0; k < tpp->inUse; k++) {
          if ((tpp->users[k].clntId == id) && (tpp->users[k].timeout <= 0)) {
            /* found client identifier */
            break;
          }
        }
        /* decrease in use count */
        if (k < tpp->inUse) {
          tpp->inUse--;
          if (tpp->inUse <= 0) {
            tpp->id = 0;
            update_active_testpoints(tpinterface, j, 0);
          }
        }
        /* shift array */
        if (k < tpp->inUse) {
          memmove(tpp->users + k, tpp->users + k + 1,
                  (tpp->inUse - k) * sizeof(tpUsage_t));
        }
        break;
      }
    }
  }
  post_active_block();

  /* release server mutex */
  G_UNLOCK(servermux);

  /* check if preselect */
  if (clearall) {
    setPreselect(node);
  }

  *result = 0;
  return TRUE;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Remote Procedure Name: querytp_1_svc					*/
/*                                                         		*/
/* Procedure Description: queries test point interface			*/
/*                                                         		*/
/* Procedure Arguments: request ID, max length of tp list		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if not			*/
/*                    test point list		          		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
bool_t querytp_1_svc(int id, int node, int tpinterface, int tplen, u_long _time,
                     int _epoch, resultQueryTP_r *result,
                     struct svc_req *rqstp) {
  static testpoint_block testpoints;
  gdsDebug("query test point");
  memset(result, 0, sizeof(resultQueryTP_r));

  int final_len = (tplen < DAQ_GDS_MAX_TP_NUM) ? tplen : DAQ_GDS_MAX_TP_NUM;

  /* test node ID */
  if ((id < 0) || (node < 0) || (node >= TP_MAX_NODE) || !tpNode.valid ||
      tpNode.node != node) {
    result->status = -2;
    return TRUE;
  }

  /* allocate memory for result array */
  result->tp.TP_r_val = malloc(final_len * sizeof(result->tp.TP_r_val[0]));
  if (result->tp.TP_r_val == NULL) {
    gdsDebug(
        "querytp_1_svc malloc(tplen * sizeof(testpoint_t)) [1] failed."); /* JCB
                                                                           */
    result->status = -2;
    return TRUE;
  }

  result->tp.TP_r_len = final_len;

  /* get index directly from test point client interface */
  testpoint_t *source = NULL;
  switch (tpinterface) {
  case TP_EX_INTERFACE:
    source = testpoints.excitations;
    break;
  case TP_TP_INTERFACE:
    source = testpoints.testpoints;
    break;
  }
  if (NULL == source) {
    fprintf(stderr, "Error: Unknown test interface %d\n", tpinterface);
    gdsDebug("querytp_1_svc unknown testpoint interface");
    result->status = -3;
    return TRUE;
  }

  if (NULL != read_testpoints && read_testpoints(&testpoints)) {
    result->status = final_len;

    for (int i = 0; i < result->tp.TP_r_len; ++i) {
      result->tp.TP_r_val[i] = source[i];
    }
  } else {
    free(result->tp.TP_r_val);
    result->tp.TP_r_val = NULL;
    result->tp.TP_r_len = 0;
  };

  return TRUE;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Remote Procedure Name: keepalive_1_svc				*/
/*                                                         		*/
/* Procedure Description: keep alive function				*/
/* This function is used as follows: Clients which do not support a	*/
/* keep alive call it once during initialization with the value -2;	*/
/* the function returns a handle greater than the max. index of the	*/
/* client list. Clients which support a keep alive call this function	*/
/* once during initialization with the value -1; and then every 15s	*/
/* with the returned handle from the first call. In this case the 	*/
/* the returned handle is an index into the client list. If the function*/
/* is not called for more than 60s, all test points associated with this*/
/* handle are deleted. A negative return value	indicates an error	*/
/* and the client should shut down. Client list entries which have	*/
/* expired are kept around for another 240s before they are deleted.	*/
/* 									*/
/* Procedure Arguments: request ID, return status, service		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if not			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
bool_t keepalive_1_svc(int id, int *ret, struct svc_req *rqstp) {
  int i;               /* index into client list */
  struct in_addr addr; /* client address */

#ifdef DEBUG
  char buf[256];
  rpcGetClientaddress(rqstp->rq_xprt, &addr);
  sprintf(buf, "keep alive %i from %s", id, inet_ntoa(addr));
  gdsDebug(buf);
  printf("%s\n", buf);
#endif

  G_LOCK(servermux);
  if (id < -1) {
    *ret = tpclntID++;
  } else if (id < 0) {
    rpcGetClientaddress(rqstp->rq_xprt, &addr);
    *ret = allocate_client_id();

    if (*ret >= 0) {
      G_UNLOCK(servermux);
      {
        time_t now = time(NULL);
#ifdef DEBUG
        printf("Allocate new TP handle %i by %s at %s\n", i, inet_ntoa(addr),
               ctime(&now));
#endif
        fflush(stdout);
      }
      return TRUE;
    }
    printf("Failed to allocate TP handle by %s\n", inet_ntoa(addr));
    *ret = -1;
  } else if ((id >= _TP_MAX_USER) || (!tpclnts[id].valid)) {
    *ret = -1;
  } else {
    tpclnts[id].lastTime = GPSnow_ns();
    *ret = id;
  }
  G_UNLOCK(servermux);
  return TRUE;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rtestpoint_1_freeresult			*/
/*                                                         		*/
/* Procedure Description: frees memory of rpc call			*/
/*                                                         		*/
/* Procedure Arguments: rpc transport info, xdr routine for result,	*/
/*			pointer to result				*/
/*                                                         		*/
/* Procedure Returns: TRUE if successful, FALSE if failed		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int rtestpoint_1_freeresult(SVCXPRT *transp, xdrproc_t xdr_result,
                            caddr_t result) {
  (void)xdr_free(xdr_result, result);
  return TRUE;
}