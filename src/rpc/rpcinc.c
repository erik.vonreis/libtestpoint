static char *versionId = "Version $Id$";
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: rpcinc							*/
/*                                                         		*/
/* Module Description: implements functions used by the gds rpc 	*/
/* interface								*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* Header File List: */
#ifndef __EXTENSIONS__
#define __EXTENSIONS__
#endif
#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif
#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>

#define _RPC_HDR
#define _RPC_XDR
#include "rpcinc.h"

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Constants: _NETID		  net protocol used for rpc		*/
/*            _SHUTDOWN_DELAY	  wait period before shut down		*/
/*            SIG_PF		  signal function prototype 		*/
/*            _IDLE		  server idle state			*/
/*            _BUSY		  server busy state			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#define _NETID "tcp"
#define _SHUTDOWN_DELAY 120 /* 120 s */
#ifndef SIG_PF
#define SIG_PF void (*)(int)
#endif
#define _IDLE 0
#define _BUSY 1
#define _INIT_SLEEP 1000000UL /*   1 ms */

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Types: rpcCBService_t	rpc service task argument		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
struct rpcCBService_t {
  u_long *prognum;
  u_long progver;
  SVCXPRT **transport;
  rpc_dispatch_t dispatch;
};
typedef struct rpcCBService_t rpcCBService_t;

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Globals: servermux		protects globals			*/
/*          serverState		state of server		 		*/
/*          shutdown		shutdown flag for port monitor serveres	*/
/*          runsvc		1 if rpc svc already runnning		*/
/*          defaultServerMode	the default server mode			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

static int runsvc = 0;
FILE *gdslog;

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: __gethostbyname_r				*/
/*            								*/
/*----------------------------------------------------------------------*/
static struct hostent *__gethostbyname_r(const char *name,
                                         struct hostent *result, char *buffer,
                                         int buflen, int *h_errnop) {
  struct hostent *ret = 0;
  if (gethostbyname_r(name, result, buffer, buflen, &ret, h_errnop) < 0) {
    return 0;
  } else {
    return ret;
  }
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: nslookup					*/
/*                                                         		*/
/* Procedure Description: looks up a hostname (uses DNS if necessary)	*/
/*                                                         		*/
/* Procedure Arguments: host address, address (return)			*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static int nslookup(const char *host, struct in_addr *addr) {
  char hostname[256];
  struct hostent hostinfo;
  struct hostent *phostinfo;
  char buf[2048];
  int i;

  if (addr == NULL) {
    return -1;
  }
  if (host == NULL) {
    if (gethostname(hostname, sizeof(hostname)) < 0) {
      return -1;
    }
  } else {
    strncpy(hostname, host, sizeof(hostname));
  }

  phostinfo = __gethostbyname_r(hostname, &hostinfo, buf, 2048, &i);
  if (phostinfo == NULL) {
    return -1;
  } else {
    memcpy(&addr->s_addr, hostinfo.h_addr_list[0], sizeof(addr->s_addr));
    return 0;
  }
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcGetHostaddress				*/
/*                                                         		*/
/* Procedure Description: returns a host address given the host name	*/
/*                                                         		*/
/* Procedure Arguments: hostname, inet address (return value)		*/
/*                                                         		*/
/* Procedure Returns: 0 if succesful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int rpcGetHostaddress(const char *hostname, struct in_addr *addr) {
  if (hostname == NULL) {
    addr->s_addr = inet_addr("127.0.0.1");
    return 0;
  }

  /* determine host address */
  /* use DNS if necessary */
  return nslookup(hostname, addr);
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcGetLocaladdress				*/
/*                                                         		*/
/* Procedure Description: returns the local address 			*/
/*                                                         		*/
/* Procedure Arguments: inet address (return value)			*/
/*                                                         		*/
/* Procedure Returns: 0 if succesful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int rpcGetLocaladdress(struct in_addr *addr) {
  char myname[256]; /* local host name */

  if ((gethostname(myname, sizeof(myname)) != 0) ||
      (rpcGetHostaddress(myname, addr) != 0)) {
    return -1;
  } else {
    return 0;
  }
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcGetClientaddress				*/
/*                                                         		*/
/* Procedure Description: returns a client address of the rpc call	*/
/*                                                         		*/
/* Procedure Arguments: service handle, inet address (return value)	*/
/*                                                         		*/
/* Procedure Returns: 0 if succesful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int rpcGetClientaddress(const SVCXPRT *xprt, struct in_addr *clntaddr) {
  /* determine network address client address */
  {
    struct sockaddr_in *addr;
    addr = svc_getcaller((SVCXPRT *)xprt);
    *clntaddr = addr->sin_addr;
  }

  return 0;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcInitializeServer				*/
/*                                                         		*/
/* Procedure Description: initializes rpc server stuff			*/
/*                                                         		*/
/* Procedure Arguments: rpcpmstart - pointer to port monitor boolean	*/
/*                      svc_fg - whether to start service in foreground	*/
/*                      svc_mode - sets rpc service mt mode		*/
/*                      transp - returned transport handle		*/
/*                      proto - returned protocol type			*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int rpcInitializeServer(int svc_fg, int svc_mode, SVCXPRT **transp,
                        int *proto) {
  /* ignore pipe signals */
  struct sigaction sa;
  sa.sa_flags = 0;
  sa.sa_handler = SIG_IGN;
  sigaction(SIGPIPE, &sa, NULL);
  // (void) sigset (SIGPIPE, SIG_IGN);

  /* fork on UNIX machines */
  if (svc_fg == 0) {
    int size;
    struct rlimit rl;
    int pid; /* process id */
    int i;

    /* try to fork */
    pid = fork();
    if (pid < 0) {
      /* error */
      fprintf(stderr, "cannot fork");
      return -4;
    }
    /* exit if parent */
    if (pid != 0) {
      exit(0);
    }
    /* child process starte here: close all unnecessary files */
    rl.rlim_max = 0;
    getrlimit(RLIMIT_NOFILE, &rl);
    if ((size = rl.rlim_max) == 0) {
      fprintf(stderr, "unable to close file handles");
      return -5;
    }
    for (i = 0; i < size; i++) {
      close(i);
    }
    i = open("/dev/null", 2);
    (void)dup2(i, 1);
    (void)dup2(i, 2);
    setsid();
    openlog("gdsrsched", LOG_PID, LOG_DAEMON);
  }

  /* create transport */
  /* port map service */
  {
    int sock; /* socket handle */

    sock = RPC_ANYSOCK;
    *proto = IPPROTO_TCP;

    /* create rpc service handle: tcp only */
    *transp = svctcp_create(sock, 0, 0);
    if (*transp == NULL) {
      fprintf(stderr, "cannot create tcp service");
      return -6;
    }
  }

  return 0;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcStartServer				*/
/*                                                         		*/
/* Procedure Description: starts an rpc server				*/
/*                                                         		*/
/* Procedure Arguments: rpcpmstart - port monitor boolean		*/
/*                      shutdown - shutdown if zero	 		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
void rpcStartServer() {
  /* wait for rpc calls */
  svc_run();
  /* never reached */
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcRegisterService				*/
/*                                                         		*/
/* Procedure Description: registers an rpc service			*/
/*                                                         		*/
/* Procedure Arguments: rpcpmstart - port monitor boolean		*/
/*                      transp - transport handle			*/
/*                      proto - protocol type				*/
/*                      prognum - rpc program number 			*/
/*                      progver - rpc version number			*/
/*                      service - service function			*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int rpcRegisterService(SVCXPRT *transp, int proto, unsigned long prognum,
                       unsigned long progver, rpc_dispatch_t service) {

  /* stand alone */
  (void)pmap_unset(prognum, progver);
  if (!svc_register(transp, prognum, progver, service, proto)) {
    fprintf(stderr, "unable to create rpc service");
    return -1;
  }

  return 0;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: rpcCBService				*/
/*                                                         		*/
/* Procedure Description: rpc callback service				*/
/*                                                         		*/
/* Procedure Arguments: scheduler descriptor				*/
/*                                                         		*/
/* Procedure Returns: void (sets callbacknum if successful)		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static void rpcCBService(rpcCBService_t *arg) {
  u_long callbacknum;

  /* initialize rpc for VxWorks */

  /* register service */
  if (rpcRegisterCallback(&callbacknum, arg->progver, arg->transport,
                          arg->dispatch) != 0) {
    *arg->prognum = (u_long)-1;
    return;
  }
  /* calback is registered */
  *arg->prognum = callbacknum;

  /* invoke rpc service; never to return */
  if (runsvc > 0) {
    return;
  }
  runsvc = 1;
  svc_run();
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcRegisterCallback				*/
/*                                                         		*/
/* Procedure Description: registers a callback rpc interface		*/
/*                                                         		*/
/* Procedure Arguments: dispatch function, pointer to store program 	*/
/*			number, version number, transport id		*/
/*                                                         		*/
/* Procedure Returns: service handle if successful, NULL if failed	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int rpcRegisterCallback(u_long *program, u_long version, SVCXPRT **transport,
                        rpc_dispatch_t dispatch) {
  SVCXPRT *transp; /* transport handle */
  u_long prognum;  /* transient rpc program # */

  /* TS-RPC calls */
  {
    /* create transport handle */
    transp = svctcp_create(RPC_ANYSOCK, 0, 0);
    if (transp == NULL) {
      fprintf(stderr, "cannot create tcp service");
      return -1;
    }

    /* now try to register */
    prognum = 0x40000000;
    while (
        (prognum < 0x60000000) &&
        (svc_register(transp, prognum, version, dispatch, IPPROTO_TCP) == 0)) {
      prognum++;
    }
    if (prognum >= 0x60000000) {
      svc_destroy(transp);
      fprintf(stderr, "can't register rpc callback service");
      return -2;
    }
  }

  /* copy results and return */
  if (program != NULL) {
    *program = prognum;
  }
  if (transport != NULL) {
    *transport = transp;
  }
  return 0;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: rpcProbe					*/
/*                                                         		*/
/* Procedure Description: probes an rpc interface			*/
/*                                                         		*/
/* Procedure Arguments: hostname, prog. num, vers. num, netid, timeout 	*/
/*                      client (return)                   		*/
/*                     			                   		*/
/* Procedure Returns: 1 if successful, 0 otherwise			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int rpcProbe(const char *host, const u_long prognum, const u_long versnum,
             const char *nettype, const struct timeval *timeout,
             CLIENT **client) {
  CLIENT *clnt;

  clnt = clnt_create((char *)host, prognum, versnum, (char *)nettype);
  if (client != NULL) {
    *client = clnt;
  } else if (clnt != NULL) {
    clnt_destroy(clnt);
  }
  return (clnt != NULL);
}
