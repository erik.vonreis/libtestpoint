#include <stdio.h>

#include <testpoint/testpoint_client.h>
#include <testpoint/testpoint_interface.h>

#include "glib.h"

int main(int argc, char *argv[]) {
  int result;
  int node = 42;

  printf("secondary test client starting\n");

  testpoint_init();
  if (result = testpoint_set_host_address(node, "localhost", 0, 0)) {
    fprintf(stderr, "FAILED: testpoint_set_host_address returned %d\n", result);
    return 1;
  }

  result = testpoint_client();
  if (result != 1) {
    fprintf(stderr, "FAILED: testpoint_client() returned %d\n", result);
    return 1;
  }

  testpoint_t tp = 10002;

  testpoint_request(node, &tp, 1, -1);

  g_usleep(5000000);

  testpoint_cleanup();

  printf("secondary test client finished\n");

  return 0;
}