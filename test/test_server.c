#include <testpoint/testpoint_server.h>
#include <string.h>
#include <stdio.h>
#include <glib.h>

#ifndef G_THREADS_ENABLED
#error "g_threads are not enabled in glib.h"
#endif //G_THREAD_ENABLED
#ifdef G_THREADS_IMPL_NONE
#error "g_threads are not implemented in glib.h"
#endif //G_THREAD_ENABLED

static testpoint_block testpoints;

int read_testpoints(testpoint_block *tp) {
  memcpy(tp, &testpoints, sizeof(testpoints));
  return 1;
}

gpointer fetch_tps(gpointer data) {
  g_usleep(1000000);
  for(;;) {
    testpoint_block *new_tps = testpoint_get_latest_block(0);
    if(NULL != new_tps) {
      memcpy(&testpoints, new_tps, sizeof(testpoints));
      printf("setting new test points\n");
      testpoint_free_block(new_tps);
      g_usleep(900000);
    }
  }
}

gpointer clean_tps(gpointer data) {
  for(;;)
  {
    g_usleep(1000000);
    testpoint_server_cleanup();
  }
}

int main(int argc, char *argv[]) {
  memset(&testpoints, 0, sizeof(testpoints));
  GThread *tp_thread = g_thread_new("tp_recv", fetch_tps, NULL);
  GThread *clean_thread = g_thread_new("tp_clean", clean_tps, NULL);
  if(!testpoint_start_server("x1model1", argv[1], read_testpoints)) {
    fprintf(stderr, "failed to start server\n");
    return 1;
  }
  return 0;
}