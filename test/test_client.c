#include <stdio.h>

#include <testpoint/testpoint_client.h>
#include <testpoint/testpoint_interface.h>

#include "glib.h"

#define countof(A) (sizeof(A)/sizeof(A[0]))

/**
 * @brief Checks if the test points on a given interface match the expected values.
 *
 * This function compares the test points on a specific interface of a given node against
 * the expected test points provided in a set. It retrieves the test points using the
 * testpoint_query() function, and then compares them with the expected set. If the actual
 * test points do not match the expected set, the function returns 0 and prints an error message.
 *
 * @param node The node for which to check the interface.
 * @param interface The interface to check on the node.
 * @param tp_set The expected test points set.
 * @param num_set The number of test points in the expected set.
 * @return Returns 1 if the actual test points match the expected set, 0 otherwise.
 */
int check_interface(const char *name, int node, int interface, testpoint_t tp_set[], int num_set) {
  testpoint_t tp[40];
  if(num_set > countof(tp))
  {
    fprintf(stderr, "ERROR: [%s] num_set=%d but cannot be bigger than %d\n",
            name, num_set, countof(tp));
    return 0;
  }
  int len = testpoint_query(node, interface, tp, countof(tp));
  if(len < countof(tp)) {
    fprintf(stderr, "FAILED: [%s] testpoint_query only returned %d testpoints\n",
            name, len);
    return 0;
  }
  if(len > countof(tp)) {
    fprintf(stderr, "FAILED: [%s] testpoint_query returned %d testpoints, "
                    "which overflows the buffer.\n", name, len);
    return 0;
  }
//  for (int i = 0; i < len; i++) {
//    printf("Test Point %d: %d\n", i, tp[i]);
//  }
  for (int i=0; i<num_set; i++) {
    if(tp[i] != tp_set[i]) {
      fprintf(stderr, "FAILED: [%s] the testpoint[%d] on interface %d should be %d"
                      " but was %d\n", name, i, interface, tp_set[i], tp[i]);
      return 0;
    }
  }
  for (int i=num_set; i < len; i++) {
    if(tp[i] != 0) {
      fprintf(stderr, "FAILED: [%s] the testpoint[%d] on interface %d should be zero,"
                      " but was %d\n", name, i, interface, tp[i]);
      return 0;
    }
  }
  return 1;
}

int secondary_done = 0;
int secondary_wait_status = -100;

void watch_secondary(GPid pid, gint wait_status, gpointer user_data) {
  printf("Secondary test client exited with code %d\n", wait_status);
  secondary_wait_status = wait_status;
  secondary_done = 1;
  g_spawn_close_pid(pid);
  GMainLoop *loop = (GMainLoop *)user_data;
  g_main_loop_quit(loop);
}

gboolean wait_secondary_timeout(gpointer loop_raw) {
  printf("Timed out waiting for seondary test client to finish.\n");
  GMainLoop *loop = (GMainLoop *)loop_raw;
  g_main_loop_quit(loop);
}


int main(int argc, char *argv[]) {
  int result;
  int node = 42;

  char test_bin_dir[1024];

  printf("running %s\n", argv[0]);

  char *last_slash = strrchr(argv[0], '/');
  if (last_slash != NULL) {
    strncpy(test_bin_dir, argv[0], last_slash - argv[0]);
    test_bin_dir[last_slash - argv[0]] = '\0';
  } else {
    test_bin_dir[0] = '.';
  }

  printf("test bin dir = %s\n", test_bin_dir);

  testpoint_init();
  if(result=testpoint_set_host_address(node, "localhost", 0, 0))
  {
    fprintf(stderr, "FAILED: testpoint_set_host_address returned %d\n", result);
    return 1;
  }

  result = testpoint_client();
  if(result != 1) {
    fprintf(stderr, "FAILED: testpoint_client() returned %d\n", result);
    return 1;
  }

  result = testpoint_clear(node, NULL, 0);
  if(result < 0) {
    fprintf(stderr, "FAILED: testpoint_clear all returned %d\n", result);
    return 1;
  }
  g_usleep(1000000);

// check excitation test points
  if(!check_interface("clear all ex", node, TP_EX_INTERFACE, NULL, 0)) {
    return 1;
  }

  if(!check_interface("clear all tp", node, TP_TP_INTERFACE, NULL, 0)) {
    return 1;
  }

  testpoint_t tp_set[20];
  tp_set[0] = 1;
  if(check_interface("should fail", node, TP_EX_INTERFACE, tp_set, 1)) {
    fprintf(stderr, "ERROR: on line %d, "
                    "check_interface should have returned an error\n", __LINE__);
    return 1;
  }

  testpoint_t tp_target[20];
  tp_target[0] = 1;
  testpoint_request(node, tp_target, 1, -1);

  g_usleep(1000000);

  if(!check_interface("set ex", node, TP_EX_INTERFACE, tp_target, 1)) {
    return 1;
  }

  result = testpoint_clear(node, tp_target, 1);
  if(result < 0) {
    fprintf(stderr, "FAILED: testpoint_clear one returned %d\n", result);
    return 1;
  }
  g_usleep(1000000);
  // check excitation test points
  if(!check_interface("clear one ex", node, TP_EX_INTERFACE, NULL, 0)) {
    return 1;
  }

  // set a regular test point
  tp_target[0] = 10001;
  testpoint_request(node, tp_target, 1, -1);

  g_usleep(1000000);

  if(!check_interface("set tp", node, TP_TP_INTERFACE, tp_target, 1)) {
    return 1;
  }

  result = testpoint_clear(node, tp_target, 1);
  if(result < 0) {
    fprintf(stderr, "FAILED: testpoint_clear one returned %d\n", result);
    return 1;
  }
  g_usleep(1000000);
  // check excitation test points
  if(!check_interface("clear one ex", node, TP_TP_INTERFACE, NULL, 0)) {
    return 1;
  }

  // check multi-set
  tp_target[0] = 1;
  tp_target[1] = 2;
  tp_target[2] = 3;
  tp_target[3] = 10001;
  tp_target[4] = 10002;
  tp_target[5] = 10003;

  result = testpoint_request(node, tp_target, 6, -1);
  if(result != 0) {
    fprintf(stderr, "FAILED: expect return of 0 for multi request.  Got %d\n", result);
    return 1;
  }
  g_usleep(1000000);
  if(!check_interface("multi req ex", node, TP_EX_INTERFACE, tp_target, 2))
  {
    return 1;
  }
  if(!check_interface("multi req tp", node, TP_TP_INTERFACE, tp_target+3, 2))
  {
    return 1;
  }

  result = testpoint_clear_name("X1:MOD_FOO_TP");
  g_usleep(1000000);
  if(result) {
    fprintf(stderr, "FAILED: testoint_clear_name returned %d.\n", result);
    return 1;
  }
  if(!check_interface("clear name", node, TP_TP_INTERFACE, tp_target+3, 1))
  {
    return 1;
  }

  testpoint_request_name("X1:MOD_FOO_TP", -1);
  g_usleep(1000000);
  if(!check_interface("req name", node, TP_TP_INTERFACE, tp_target+3, 2))
  {
    return 1;
  }


  // test two clients at the same time
  result = testpoint_clear_name("X1:MOD_FOO_TP");
  g_usleep(1000000);
  if(result) {
    fprintf(stderr, "FAILED: testoint_clear_name returned %d.\n", result);
    return 1;
  }
  if(!check_interface("clear name2", node, TP_TP_INTERFACE, tp_target+3, 1))
  {
    return 1;
  }


  gchar *spawn_argv[] = {"secondary_test_client", 0};
  GPid secondary_pid;
  GSpawnFlags flags = G_SPAWN_DO_NOT_REAP_CHILD;
  GError *g_error = NULL;
  printf("Spawning secondary test client\n");
  gboolean spawn_result = g_spawn_async(test_bin_dir, spawn_argv, NULL, flags,
                NULL, NULL, &secondary_pid,  &g_error);
  if(!spawn_result) {
    fprintf(stderr, "FAILED: could not spawn secondary test client: [%d:%d] %s.\n",
            g_error->domain, g_error->code, g_error->message);
    return 1;
  }

  GMainLoop *loop;
  loop = g_main_loop_new(NULL, FALSE);

  g_child_watch_add(secondary_pid, watch_secondary, loop);

  g_usleep(1000000);

  int ret = 0;

  if(!check_interface("sec req", node, TP_TP_INTERFACE, tp_target+3, 2))
  {
    ret = 1;
  }

  testpoint_request_name("X1:MOD_FOO_TP", -1);

  result = testpoint_clear_name("X1:MOD_FOO_TP");
  g_usleep(1000000);
  if(result) {
    fprintf(stderr, "FAILED: testpoint_clear_name returned %d.\n", result);
    ret =1;
  }
  if(!check_interface("sec hold", node, TP_TP_INTERFACE, tp_target+3, 2))
  {
    ret = 1;
  }

  g_timeout_add_seconds(10, wait_secondary_timeout, loop);

  g_main_loop_run(loop);
  g_main_loop_unref(loop);

  if(secondary_wait_status == -100) {
    fprintf(stderr, "FAILED: secondary test client did not exit in time.\n");
    ret = 1;
  }
  else if(secondary_wait_status != 0) {
    fprintf(stderr, "FAILED: secondary test client exited with code %d\n", secondary_wait_status);
    ret = 1;
  }

  if(ret != 0) {
    return ret;
  }

  if(!check_interface("sec closed", node, TP_TP_INTERFACE, tp_target+3, 1))
  {
    fprintf(stderr, "WARNING client is known to not automatically close testpoints on exit.\n");
  }

  testpoint_cleanup();
  printf("passed all tests!\n");
  return 0;
}