//
// Created by erik.vonreis on 9/27/23.
//

#ifndef LIBTESTPOINT_TESTPOINT_COMMON_H
#define LIBTESTPOINT_TESTPOINT_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get the library version
 * @param major_version
 * @param minor_version
 * @param release_version
 */
void testpoint_version(int *major_version, int *minor_version,
                       int *release_version);

/**
 *
 * Check that the library is compatible with
 * major_version.minor_version.release_version
 *
 * 'compatible' means the given version is <= the library version, and the given
 * version is ABI compatible.
 *
 * If the version is compatible, "NULL" is returned.
 *
 * If not compatible, a string is returned with details.
 * The string is owned by the library and should not be freed.
 *
 * @param major_version
 * @param minor_version
 * @param release_version
 * @return
 */
const char *testpoint_is_compatible_with(int major_version, int minor_version,
                                         int release_version);

/**
 * Report the preferred network interface version.
 * Other version may be supported.
 * @return
 */
int testpoint_network_interface_version();

#ifdef __cplusplus
}
#endif

#endif // LIBTESTPOINT_TESTPOINT_COMMON_H
