//
// Created by erik.vonreis on 5/21/21.
//

#ifndef DAQD_TRUNK_TESTPOINT_H
#define DAQD_TRUNK_TESTPOINT_H

#include <cds/cdstime.h>

#include "testpoint_data.h"
#include "testpoint_interface.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
extern "C" {
#endif

/** Sets the test point manager address. This function is only
    available when compiling with the _TP_DAQD flag. It will disable
    the default parameter file and set the host address and rpc program
    and version number directly. If a program number of zero is
    specified, the default will be used. If a program version of
    zero is specified, the default will be used. This function must
    be called before any other function (including testpoint_client).

    @param node test point node
    @param hostname host name of test point manager
    @param prognum rpc program number
    @param progver rpc program version
    @return 0 if successful, <0 otherwise
    @author DS, June 98
************************************************************************/
int testpoint_set_host_address(int node, const char *hostname,
                               unsigned long prognum, unsigned long progver);

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: initTestpoint				*/
/*                                                         		*/
/* Procedure Description: initializes test point interface		*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
void testpoint_init(void);

/**
 * Loads the testpoint configuration dynamically using libgdsconf.
 * Either this function needs to be called once, or testpoint_set_host_address
 * needs to manually configure each node that will be used.
 *
 * If this function is called, call it after testpoint_init(), but before any
 * other testpoint function is called.
 */
void testpoint_load_from_config();

/** Installs a test point client interface. This function should be
    called prior of using the test point interface. If not, the first
    call to any of the functions in this API will call it. However,
    it may take up to two seconds, before the index cache is filled
    and the interface is working properly.

    @param void
    @return 0 if successful, <0 otherwise
    @author DS, June 98
************************************************************************/
int testpoint_client(void);

/** Clear test points. This routine clears test points from the
    reflective memory. Test points are cleared in the 6th epoch only.
    If the pointer to the test point list is NULL, all test points
    of this node are cleared. This routine returns immediately.

    @param node test point node
    @param tp list of test point ID numbers
    @param tplen length of test point list
    @return 0 if successful, < 0 otherwise
    @author DS, June 98
************************************************************************/
int testpoint_clear(int node, const testpoint_t tp[], int tplen);

/** Terminates a test point client interface.

    @param void
    @return void
    @author DS, June 98
************************************************************************/
void testpoint_cleanup(void);

/** Request test points. The requested test points are made available
    in the reflective memory if enough test point slots are free.
    Each test point has a unique ID which must be defined in the
    channel information block of the reflective memory. An optional
    timeout can be specified to automatically clear a test point after
    a given time (-1 means forever). Test points are set in the 8th
    epoch only. If successful, this routine returns the number of newly
    set test points and a negative error number otherwise. Optionally,
    the time and epoch when the test points become active can be returned.
    If more test points are requested than there are free slots, the
    routine will allocate as many test points as possible. Test points
    which are allocated with a timeout must not be cleared.

    @param node test point node
    @param tp list of test point ID numbers
    @param tplen length of test point list
    @param timeout maximum time (in nsec) the test point is needed
    @param time time (in sec) when the test point becomes active
    @param epoch epoch when the test point becomes active
    @return 0 if successful, <0 otherwise
    @author DS, June 98
************************************************************************/
int testpoint_request(int node, const testpoint_t tp[], int tplen,
                      gps_ns_t timeout);

/** Request test points by name. This routine is similar to
    tpRequest but accepts channel names of test points. The tpNames
    argument can contain a list of space separated channel names.
    Limitation: Sends the test point names to the test point
    manager of node 0 only.

    @param node test point node
    @param tpNames space seperated list of channel names
    @param timeout maximum time (in nsec) the test point is needed
    @param time time (in sec) when the test point becomes active
    @param epoch epoch when the test point becomes active
    @return 0 if successful, <0 otherwise
    @author DS, June 98
************************************************************************/
int testpoint_request_name(const char *tpNames, gps_ns_t timeout);

/** Clear test points by names. This routine is similar to tpClear but
    accepts channel names of test points. The tpNames
    argument can contain a list of space separated channel names.
    Limitation: Sends the test point names to the test point
    manager of node 0 only.

    @param node test point node
    @param tpNames space seperated list of channel names
    @return 0 if successful, <0 otherwise
    @author DS, June 98
************************************************************************/
int testpoint_clear_name(const char *tpNames);

/** Query the test point interface. This routine returns a list of
    all assigned test points in the reflective memory belonging to the
    specified node/interface combination. The position within the list
    corresponds to the slot number. A zero indicates an unused slot.
    To avoid ambiguities around the time when the test point list
    changes, the routine takes the requested time and epoch as
    additional arguments. If the time is too far in the future, the
    routine will return the most recent test point information, rather
    than wait. If the time is too far in the past, it will return an
    error. If both time and epoch are zero, the current test point
    information is returned. However, this may already be out-dated
    when the function returns.

    If running on a front-end CPU this routine will retrieve the
    information directly from the reflective memory (assuming
    the node is accessible and the _TESTPOINT_DIRECT flag was
    specified during compilation). The number of read test point is
    returned if successful. If the test point list is NULL, nothing
    is copied, but the routine will still return the number of test
    points in the index. This routine will try to query a remote
    test point server if the test points aren't accessible locally.

    @param node test point node
    @param tpinterface testpoint interface id
    @param tp list of test point ID numbers
    @param tplen length of test point list
    @param time time of query request
    @param epoch epoch of query request
    @return Number of read test points if successful, <0 otherwise
    @author DS, June 98
************************************************************************/
int testpoint_query(int node, int tpinterface, testpoint_t tp[], int tplen);

/** ASCII interface to a test point interface.

    The function returns a string which was allocated with malloc. The
    caller is reposnible to free this string if no longer needed.

    @param cmd command string
    @return reply string
    @author DS, June 98
************************************************************************/
char *testpoint_command(const char *cmd);

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: keepAlive					*/
/*                                                         		*/
/* Procedure Description: sends keep alive				*/
/*                                                         		*/
/* Procedure Arguments: scheduler task, time, epoch, argument		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 if not			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int testpoint_keep_alive(int node);

int testpoint_keep_alive_thread(int node);

/// Find RPC interface version used by awgtpman
/// version 4 was introduced about RCG version 4.1.0
/// all previous interface versions, including for RCG 4.0.1,
/// are considered version 3.
/// Version 4 has only two testpoint interfaces: one for excitations and one for
/// other test points.
/// Version 3 has 4 testpoint interfaces: the two interfaces of version 4 are
/// divided between LSC and ASC test points.
///
/// \param node the node number to identify
/// \return the version number, 0 on error
int testpoint_interface_version(int node);

#ifdef __cplusplus
}
#endif

#endif // DAQD_TRUNK_TESTPOINT_H
