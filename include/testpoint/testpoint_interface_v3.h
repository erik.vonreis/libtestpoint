//
// Created by erik.vonreis on 5/4/22.
//
// defines needed for older version 3 RPC interface, stolen from
// version 4.0.1
//

#ifndef DAQD_TRUNK_TESTPOINT_INTERFACE_V3_H
#define DAQD_TRUNK_TESTPOINT_INTERFACE_V3_H

/** Test point interface id of LSC excitation.

@author DS, September 98
    @see Testpoint Definition
        ************************************************************************/
#define TP_LSC_EX_INTERFACE 0

/** Test point interface id of ASC excitation.

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_ASC_EX_INTERFACE 1

/** Test point interface id of LSC readout.

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_LSC_TP_INTERFACE 2

/** Test point interface id of ASC readout.

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_ASC_TP_INTERFACE 3

#define TP_MAX_INTERFACE_V3 4

#define TP_ID_TO_INTERFACE_V3(A)                                               \
  (((A) == 0 ? -1                                                              \
             : ((A) < TP_ID_LSC_TP_OFS                                         \
                    ? TP_LSC_EX_INTERFACE                                      \
                    : ((A) < TP_ID_ASC_EX_OFS                                  \
                           ? TP_LSC_TP_INTERFACE                               \
                           : ((A) < TP_ID_ASC_TP_OFS                           \
                                  ? TP_ASC_EX_INTERFACE                        \
                                  : ((A) < TP_ID_DAC_OFS                       \
                                         ? TP_ASC_TP_INTERFACE                 \
                                         : ((A) < TP_ID_DS340_OFS              \
                                                ? TP_DAC_INTERFACE             \
                                                : ((A) < TP_ID_END_OFS         \
                                                       ? TP_DS340_INTERFACE    \
                                                       : -1))))))))

/** Maximum number of entries in the test point index. This number must
    be greater or equal than any of the individual test point indexes.

@author DS, September 98
    @see Testpoint Definition
        ************************************************************************/
#define TP_MAX_INDEX_V3 64

#endif // DAQD_TRUNK_TESTPOINT_INTERFACE_V3_H
