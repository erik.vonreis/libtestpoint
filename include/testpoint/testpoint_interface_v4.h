//
// Created by erik.vonreis on 9/27/23.
//

#ifndef LIBTESTPOINT_TESTPOINT_INTERFACE_V4_H
#define LIBTESTPOINT_TESTPOINT_INTERFACE_V4_H

/** Excitation test point interface id

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_EX_INTERFACE 0

/** Test point interface id

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_TP_INTERFACE 1

#define TP_MAX_INTERFACE_V4 2

#define TP_ID_TO_INTERFACE_V4(A)                                               \
  (((A) == 0 ? -1                                                              \
             : ((A) < TP_ID_LSC_TP_OFS                                         \
                    ? TP_EX_INTERFACE                                          \
                    : ((A) < TP_ID_ASC_EX_OFS                                  \
                           ? TP_TP_INTERFACE                                   \
                           : ((A) < TP_ID_ASC_TP_OFS                           \
                                  ? TP_EX_INTERFACE                            \
                                  : ((A) < TP_ID_DAC_OFS                       \
                                         ? TP_TP_INTERFACE                     \
                                         : ((A) < TP_ID_DS340_OFS              \
                                                ? TP_DAC_INTERFACE             \
                                                : ((A) < TP_ID_END_OFS         \
                                                       ? TP_DS340_INTERFACE    \
                                                       : -1))))))))

#define TP_MAX_INDEX_V4 64

#endif // LIBTESTPOINT_TESTPOINT_INTERFACE_V4_H
