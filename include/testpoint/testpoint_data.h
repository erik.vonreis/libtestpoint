//
// Created by erik.vonreis on 5/19/21.
//

#ifndef DAQD_TRUNK_TESTPOINT_DATA_H
#define DAQD_TRUNK_TESTPOINT_DATA_H

#include <stdint.h>

typedef uint32_t testpoint_t;

#define DAQ_GDS_MAX_TP_NUM 0x100

/**
 * Block of testpoint data used to share testpoint settings
 * between awg and the realtime system over shared memory.
 * The actual shared memory block includes a header at the front.
 * This is added in the realtime code.
 */
typedef struct testpoint_block {
  testpoint_t excitations[DAQ_GDS_MAX_TP_NUM];
  testpoint_t excitations_writeback[DAQ_GDS_MAX_TP_NUM];
  testpoint_t testpoints[DAQ_GDS_MAX_TP_NUM];
  testpoint_t testpoints_writeback[DAQ_GDS_MAX_TP_NUM];
} testpoint_block;

#endif // DAQD_TRUNK_TESTPOINT_DATA_H
