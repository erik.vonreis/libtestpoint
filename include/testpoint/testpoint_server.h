/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: testpoint_server					*/
/*                                                         		*/
/* Module Description: API for handling testpoints			*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 25June98 D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: testpoint_server.html				*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8336  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_TESTPOINT_SERVER_H
#define _GDS_TESTPOINT_SERVER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "testpoint_data.h"

/**
 * Call back used to read testpoint data from the server application.
 *
 * Returns 0 on error, non-zero if successful
 *
 */
typedef int (*testpoint_reader_callback)(testpoint_block *);

/**
   @name Test Point Server
   Remote procedure service for test point interface. This rpc
   server should run on the DAQ system controller. It exports routines
   to set and clear test points, as well as to query the interface.

   @memo rpc server for test point interface
   @author Written June 1998 by Daniel Sigg
   @see Test Point API
   @version 0.1
************************************************************************/

/*@{*/

/** Starts the network service for the test point interface. This routine
    has to be called by the DAQ/GDS system controller as part of its
    initialization after the reflective memory is setup.

    This function does not return unless there is an error starting the
    service, so should be run in its own thread.

    @param system_name  the system (model) name
    @param prm_path path to the directory that contains parameter
    @param reader_callback the library will call this function every second.
    The application should implement a callback that returns the actual set
testpoints, e.g. read the block from the shared memory.
    @return 1 if successful, 0 otherwise.
    @see Test Point API
    @author DS, June 98
************************************************************************/
int testpoint_start_server(const char *system_name, const char *prm_path,
                           testpoint_reader_callback reader_callback);

/**
 *
 */
void testpoint_stop_server(void);

/**
 * If there is a new testpoint block available (the testpoint
 * configuration has changed for some reason), return a pointer to the
 * block.
 * If no block is available, the function doesn't return until a block is
 * available or the given timeout_s time has elapsed.
 *
 *
 * The returned block should be freed with a call to testpoint_free_block()
 *
 * If the function times out without a block, NULL is returned.
 *
 * @param timeout_s can be 0 for immediate return, or negative for indefinite
 * timeout
 *
 * @return
 */
testpoint_block *testpoint_get_latest_block(int timeout_s);

/**
 * Free a block previously returned by testpoint_get_latest_block()
 * @param block
 * @return
 */
void testpoint_free_block(testpoint_block *block);

/**
 * If the testpoint is open, return its index in the testpoint table.
 * If not, then return -1.
 *
 * Useful because in some systems the channel excited by an AWG slot is
 * determined by the corresponding
 * @param tp
 * @return
 */
int testpoint_get_index(testpoint_t tp);

/**
 *
 * @return the server node (DCUID)
 */
int testpoint_get_node();

/**
 * Garbage colletor for the testpoint server.
 *
 * The application should call this once per second.
 * @return
 */
int testpoint_server_cleanup(void);

/*@}*/

#ifdef __cplusplus
}
#endif

#endif /*_GDS_TESTPOINT_SERVER_H */
