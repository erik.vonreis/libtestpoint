/* Version: $Id$ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: rmorg							*/
/*                                                         		*/
/* Module Description: 	Reflective memory organization			*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Sep98   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: gds html pages					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_RMORG_H
#define _GDS_RMORG_H

#ifdef __cplusplus
extern "C" {
#endif

/** Size of test point channel data element. This number is in bytes.

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_DATUM_LEN sizeof(float)

/** Maximum number of test point nodes. This is identical to the number
    of reflective memory rings. UNIX target has command line argument
    to set the node id.

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_MAX_NODE 256

/** Highest test point index number.

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_HIGHEST_INDEX 60000

/** @name Identification numbers

    @author DS, September 98
    @see DAQ reflective memory organization
************************************************************************/

/** Test point interface id of the digital-to-analog converter. (Does
    not have a corresponding reflective memory region; this is a logical
    id only.)

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_DAC_INTERFACE 100

/** Test point interface id of DS340 signal generator. (Does not have
    a corresponding reflective memory region; this is a logical id only.)

    @author DS, September 98
    @see Testpoint Definition
************************************************************************/
#define TP_DS340_INTERFACE 101

/** Defines the test point ID offset for the LSC system.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define TP_ID_LSC_TP_OFS 10000

/** Defines the test point ID offset for the ASC excitation engine.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define TP_ID_ASC_EX_OFS 20000

/** Defines the test point ID offset for the ASC system.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define TP_ID_ASC_TP_OFS 30000

/** Defines the test point ID offset for the DAC channels.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define TP_ID_DAC_OFS 40000

/** Defines the test point ID offset for the DS340 channels.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define TP_ID_DS340_OFS 50000

/** Defines the test point ID offset for the last TP.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define TP_ID_END_OFS 60000

/** Defines the test point ID for clearing all selected TPs.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define _TP_CLEAR_ALL ((unsigned short)(-1))

#define IS_TP(A) (A->tpNum <= 0 ? 0 : 1)

#include "testpoint_interface_v3.h"
#include "testpoint_interface_v4.h"

// set the default interface

#define TESTPOINT_DEFAULT_INTERFACE 4
#define ADD_INT(y, x) y##_V##x
#define ADD_DEF_INT(y) ADD_INT(y, 4)
#define TP_MAX_INDEX ADD_DEF_INT(TP_MAX_INDEX)
#define TP_ID_TO_INTERFACE ADD_DEF_INT(TP_ID_TO_INTERFACE)
#define TP_MAX_INTERFACE ADD_DEF_INT(TP_MAX_INTERFACE)

#ifdef __cplusplus
}
#endif

#endif /*_GDS_RMORG_H */
