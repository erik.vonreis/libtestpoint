SET(CMAKE_EXPORT_COMPILE_COMMANDS true)
cmake_minimum_required(VERSION 3.13 FATAL_ERROR)

SET(CMAKE_PROJECT_HOMEPAGE_URL  "http://git.ligo.org/cds/software/libtestpoint")

project(libtestpoint VERSION 0.1.0)

enable_testing()

add_compile_definitions(
        LIBTESTPOINT_VERSION="${libtestpoint_VERSION}"
)

add_subdirectory(src)

add_subdirectory(test)

